package ru.academits.dubchak.phonebook.service;

import ru.academits.dubchak.phonebook.bean.ContactValidation;
import ru.academits.dubchak.phonebook.entity.Contact;

import java.util.List;

public interface ContactService {
    ContactValidation validateContact(Contact contact);

    ContactValidation addContact(Contact contact);

    List<Contact> getAllContacts();

    ContactValidation deleteContact(Contact contact);
}
