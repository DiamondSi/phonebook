package ru.academits.dubchak.phonebook.phonebook;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.academits.dubchak.phonebook.bean.ContactValidation;
import ru.academits.dubchak.phonebook.converter.ContactDtoToContactConverter;
import ru.academits.dubchak.phonebook.converter.ContactToContactDtoConverter;
import ru.academits.dubchak.phonebook.dto.ContactDto;
import ru.academits.dubchak.phonebook.entity.Contact;
import ru.academits.dubchak.phonebook.service.ContactService;

import java.util.List;

@RestController
@RequestMapping("/phoneBook/rcp/api/v1")
public class PhoneBookController {
    @Autowired
    private ContactService contactService;

    @Autowired
    private ContactToContactDtoConverter contactToContactDtoConverter;

    @Autowired
    private ContactDtoToContactConverter contactDtoToContactConverter;

    @GetMapping("getAllContacts")
    public List<ContactDto> getAllContacts() {
        return contactToContactDtoConverter.convert(contactService.getAllContacts());
    }

    @PostMapping("addContact")
    public ContactValidation addContact(@RequestBody ContactDto contact) {
        Contact contactEntity = contactDtoToContactConverter.convert(contact);
        return contactService.addContact(contactEntity);
    }

    @PostMapping("deleteContact")
    public ContactValidation deleteContact(@RequestBody ContactDto contact) {
        Contact contactEntity = contactDtoToContactConverter.convert(contact);
        return contactService.deleteContact(contactEntity);
    }
}
