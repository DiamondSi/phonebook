package ru.academits.dubchak.phonebook.dao;

import ru.academits.dubchak.phonebook.entity.Contact;

import java.util.List;

public interface ContactDao {
    List<Contact> getAllContacts();

    void add(Contact contact);

    void deleteById(int id);
}
