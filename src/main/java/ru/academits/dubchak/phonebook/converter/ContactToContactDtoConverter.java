package ru.academits.dubchak.phonebook.converter;

import ru.academits.dubchak.phonebook.converter.generic.GenericConverter;
import ru.academits.dubchak.phonebook.dto.ContactDto;
import ru.academits.dubchak.phonebook.entity.Contact;

public interface ContactToContactDtoConverter extends GenericConverter<Contact, ContactDto> {
}
